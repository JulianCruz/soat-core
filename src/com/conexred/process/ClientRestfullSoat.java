/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.process;

import com.brainwinner.util.LogSocketListener;
import com.conexred.util.ResponseTemplateSoat;
import com.conexred.dto.DataConnectionDTO;
import com.conexred.dto.EmisionRequestDTO;
import com.conexred.dto.EmisionResponseDTO;
import com.conexred.dto.ResponseLoginDTO;
import com.conexred.dto.RevokeRequestDTO;
import com.conexred.dto.RevokeResponseDTO;
import com.conexred.dto.RunInformationRequestDTO;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import javax.net.ssl.HttpsURLConnection;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author JCruz
 */
public class ClientRestfullSoat {

    private final String USER_AGENT = "Puntored/5.0";
    private static LogSocketListener log;
    private final String URL_BASE;
    private final String USER;
    private final String PASSWORD;

    public ClientRestfullSoat(DataConnectionDTO connectionData) {
        this.URL_BASE = connectionData.getUrl();
        this.USER = connectionData.getUsser();
        this.PASSWORD = connectionData.getPassword();
    }


    /**
     *
     * @return
     */
    public String getToken() {
        String url_service = this.URL_BASE + "login/".trim();
        int responseCode = 0;
        String responseMessage = "";
        HttpsURLConnection con = null;
        try {
            URL obj = new URL(url_service);
            con = (HttpsURLConnection) obj.openConnection();

            //Configurando el Body
            String urlParameter = "{"
                    + "\"email\":\"" + this.USER.trim() + "\","
                    + "\"password\":\"" + this.PASSWORD.trim() + "\""
                    + "}";

            //se da formato al body
            byte[] postData = urlParameter.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;

            //Configuración del header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setDoOutput(true);
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Content-Length", Integer.toString(postDataLength));

            //Envío del mensaje 
            System.out.println("\nSending 'POST' request to URL : " + url_service);

            con.getOutputStream().write(postData);

            //LLamado a la función que extrae el Json desde la respuesta
            responseCode = con.getResponseCode();
            responseMessage = con.getResponseMessage();
            System.out.println("Response Code : " + responseCode);
            System.out.println("Response Message : " + responseMessage);
            
            String responseJSON = decodeResponse(con.getInputStream());


            System.out.println("Leyendo JSON");
            ObjectMapper mapper = new ObjectMapper();
            ResponseLoginDTO login = mapper.readValue(responseJSON.trim(), ResponseLoginDTO.class);

            return login.getToken();

        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada");
                System.out.println("Response Code: " + responseCode);
                switch(responseCode){
                    case 404:
                        System.out.println("No se encuentra respuseta de soat");
                        break;
                    case 500:
                        System.out.println("Error interno en los servidores SOAT");
                        break;
                    default:
                        Scanner s = new Scanner(con.getErrorStream()).useDelimiter("\\A");
                        String erroHelp = s.hasNext() ? s.next() : "";
                        System.out.println("Response Message: " + erroHelp);
                        break;
                }
                return null;
            } catch (Exception ex2) {
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada al tramitar el error inicial");
                ex2.printStackTrace();
                return null;

            }
        }
    }

    /**
     *
     * @param query Objeto con los datos necesarios par hacer la transaccion
     * @param token Token de autenticación
     * @return
     *
     */
    public ResponseTemplateSoat getRunInformation(RunInformationRequestDTO query, String token) {
        String url_service = this.URL_BASE + "query_v2/" + query.getSale_point() + "/" + query.getRegistration() + "/" + query.getIdentification() + "/" + query.getEmail() + "/" + query.getPhone() + "/";
        HttpsURLConnection con = null;
        int responseCode = 0;
        String responseMessage = "";
        ResponseTemplateSoat response = new ResponseTemplateSoat();
        String responseJSON;
        try {
            URL obj = new URL(url_service);
            con = (HttpsURLConnection) obj.openConnection();

            //Configuración del header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setDoOutput(true);
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "jwt " + token);

            //Envío del mensaje 
            System.out.println("\nSending 'GET' request to URL : " + url_service);

            //LLamado a la función que extrae el Json desde la respuesta
            responseCode = con.getResponseCode();
            responseMessage = con.getResponseMessage();
            System.out.println("Response Code : " + responseCode);
            System.out.println("Response Message : " + responseMessage);
            responseJSON = decodeResponse(con.getInputStream());

            System.out.println("Respuesta Soat Json: " + responseJSON);
            response.setResponse(responseJSON.trim());
            response.setSuccess(true);
            return response;

        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada");
                System.out.println("Response Code: " + responseCode);
                switch(responseCode){
                    case 404:
                        System.out.println("No se encuentra respuesta de soat");
                        response.setResponse("No se encontraron datos con la informacion suministrada.");
                        response.setSuccess(false);
                        break;
                    case 500:
                        System.out.println("Error interno en los servidores SOAT");
                        response.setResponse("Soat no puede tramitar la solicitud, revise los datos e intente de nuevo");
                        response.setSuccess(false);
                        break;
                    default:
                        response.setResponse("Soat no puede tramitar la solicitud,"+responseCode);
                        response.setSuccess(false);
                        Scanner s = new Scanner(con.getErrorStream()).useDelimiter("\\A");
                        String erroHelp = s.hasNext() ? s.next() : "";
                        System.out.println("Response Message: " + erroHelp);
                        break;
                }
                return response;
            } catch (Exception ex2) {
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada al tramitar el error inicial");
                ex2.printStackTrace();
                response.setResponse("Soat no puede tramitar la solicitud,"+responseCode);
                response.setSuccess(false);
                return response;
            }
        }
    }
    
    public String getEmision(EmisionRequestDTO request,String token) {
        String url_service = this.URL_BASE + "issue/";
        HttpsURLConnection con = null;
        int responseCode = 0;
        String responseMessage;
        try {
            URL obj = new URL(url_service);
            con = (HttpsURLConnection) obj.openConnection();
            
            //Configurando el Body
            String urlParameter = "{" +
                                "  \"transaction_num\":" + request.getTransaction_num() + "," +
                                "  \"identification\":\"" + request.getIdentification() + "\"," +
                                "  \"registration\":\"" + request.getRegistration().trim() +"\"," +
                                "  \"init_date\":\"" + request.getInit_date().trim() + "\"," +
                                "  \"sale_point\":\"" + request.getSale_point().trim() + "\"" +
                                "}";
            
            //para desarrollo
            System.out.println("El body: " + urlParameter);
            
            
            //se da formato al body
            byte[] postData = urlParameter.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;

            //Configuración del header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setDoOutput(true);
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "jwt " + token);
            con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            
            //Envío del mensaje 
            System.out.println("\nSending 'POST' request to URL : " + url_service);

            con.getOutputStream().write(postData);

            responseCode = con.getResponseCode();
            responseMessage = con.getResponseMessage();
            System.out.println("Response Code : " + responseCode);
            System.out.println("Response Message : " + responseMessage);
            
            String responseJSON = decodeResponse(con.getInputStream());
            
            ObjectMapper mapper = new ObjectMapper();
            EmisionResponseDTO response = mapper.readValue(responseJSON, EmisionResponseDTO.class);
            
            System.out.println("Leyendo JSON");
            //LLamado a la función que extrae el Json desde la respuesta
            return response.getForm_num();
            
        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada");
                System.out.println("Response Code: " + responseCode);
                
                switch(responseCode){
                    case 404:
                        System.out.println("No se encuentra respuseta de soat");
                        break;
                    case 500:
                        System.out.println("Error interno en los servidores SOAT");
                        break;
                    default:
                        Scanner s = new Scanner(con.getErrorStream()).useDelimiter("\\A");
                        String erroHelp = s.hasNext() ? s.next() : "";
                        System.out.println("Response Message: " + erroHelp);
                        break;
                }
                System.out.println("Tramitando reverso...");
                
                RevokeRequestDTO reversoRequest = new RevokeRequestDTO(request.getTransaction_num(), request.getRegistration(), request.getIdentification(), request.getSale_point());
                makeRevoke(reversoRequest, token);
                System.out.println("Reverso solicitado.");
                return null;
            } catch (Exception ex2) {
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada al tramitar el error inicial");
                ex2.printStackTrace();
                return null;

            }
        }
        
    }
    
    public RevokeResponseDTO makeRevoke(RevokeRequestDTO request,String token) {
        String url_service = this.URL_BASE + "revoke/";
        HttpsURLConnection con = null;
        try {
            URL obj = new URL(url_service);
            con = (HttpsURLConnection) obj.openConnection();
            
            //Configurando el Body
            String urlParameter = "{" +
                                "  \"transaction_num\":\"" + request.getTransaction_num() + "\"," +
                                "  \"identification\":\"" + request.getIdentification() + "\"," +
                                "  \"registration\":\"" + request.getRegistration() +"\"," +
                                "  \"sale_point\":\"" + request.getSale_point() + "\"" +
                                "}";
            
            //se da formato al body
            byte[] postData = urlParameter.getBytes(StandardCharsets.UTF_8);
            int postDataLength = postData.length;

            //Configuración del header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setDoOutput(true);
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "jwt " + token);
            con.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            
            //Envío del mensaje 
            System.out.println("\nSending 'POST' request to URL : " + url_service);

            con.getOutputStream().write(postData);

            //LLamado a la función que extrae el Json desde la respuesta
            String responseJSON = decodeResponse(con.getInputStream());

            System.out.println("Response Code : " + con.getResponseCode());
            System.out.println("Response Message : " + con.getResponseMessage());

            System.out.println("Leyendo JSON");
            ObjectMapper mapper = new ObjectMapper();
            RevokeResponseDTO  response = mapper.readValue(responseJSON.trim(), RevokeResponseDTO.class);

            return response;
            
        } catch (Exception ex) {
            try {
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada");
                System.out.println("Response Code: " + con.getResponseCode());
                System.out.println("Response Message: " + con.getErrorStream());
                System.out.println("Response Content: " + decodeResponse(con.getInputStream()));
                ex.printStackTrace();
                return null;
            } catch (Exception ex2) {
                System.out.println("Exception: " + ex.getClass().getSimpleName() + " capturada al tramitar el error inicial");
                ex2.printStackTrace();
                return null;

            }
        }
        
    }

    private String decodeResponse(InputStream stream) {
        Reader in = null;
        StringBuilder sb = new StringBuilder();
        String response = sb.toString();
        try {
            in = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            for (int c; (c = in.read()) >= 0;) {
                sb.append((char) c);
            }
            response = sb.toString();
        } catch (Exception ex) {
            System.out.println("Error al decodificar: " + ex.getClass().getSimpleName());
            response = "Error: " + ex.getClass().getSimpleName();
            ex.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getClass().getSimpleName());
                ex.printStackTrace();
                response = null;
            }
        }
        return response;
    }
}
