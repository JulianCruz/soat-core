/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.process;

import com.conexred.dto.DataConnectionDTO;
import com.conexred.dto.EmisionRequestDTO;
import com.conexred.dto.RevokeRequestDTO;
import com.conexred.dto.RevokeResponseDTO;
import com.conexred.dto.RunInformationRequestDTO;
import static com.conexred.util.Constants.CODE_ERROR_EMISION;
import static com.conexred.util.Constants.CODE_ERROR_EXCEPTION;
import static com.conexred.util.Constants.CODE_ERROR_LOGIN;
import static com.conexred.util.Constants.CODE_ERROR_QUERY;
import static com.conexred.util.Constants.MESSAGE_ERROR_EXCEPTION;
import static com.conexred.util.Constants.MESSAGE_ERROR_LOGIN;
import static com.conexred.util.Constants.MESSAGE_ERROR_EXITO;
import static com.conexred.util.Constants.CODE_ERROR_EXITO;
import static com.conexred.util.Constants.CODE_ERROR_REVOKE;
import static com.conexred.util.Constants.MESSAGE_ERROR_EMISION;
import static com.conexred.util.Constants.MESSAGE_ERROR_REVOKE;
import com.conexred.util.ResponseTemplateSoat;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author JCruz
 */
public class ProcessSoat {

    private final DataConnectionDTO dataConection;
    private ClientRestfullSoat client;
    public ProcessSoat(DataConnectionDTO dataConection) {
        this.dataConection = dataConection;
        this.client = new ClientRestfullSoat(dataConection);
    }

    public ISOMsg getInfo(RunInformationRequestDTO data, ISOMsg responseTemplate) {

        ISOMsg response = responseTemplate;
        try{

            //Obtener token
            System.out.println("Obteniendo Token.... ");
            String token = client.getToken();

            //validando token
            if (token == null) {
                System.out.println("Error obteniendo Token =( .... ");
                response.set(39, CODE_ERROR_LOGIN);
                response.set(63, MESSAGE_ERROR_LOGIN);
                return response;
            }
            System.out.println("Token exitoso");

            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println("Obteniendo Informaciòn SOAT......");
            ResponseTemplateSoat infoRun = client.getRunInformation(data, token);

            if (!infoRun.isSuccess()) {
                System.out.println("Error obteniendo Informaciòn SOAT =( .... ");
                response.set(39, CODE_ERROR_QUERY);
                response.set(63, infoRun.getResponse());
                return response;
            }
            
            System.out.println("Información obtenida con exito");
            
            response.set(124, infoRun.getResponse());
            response.set(39, CODE_ERROR_EXITO);
            response.set(63, MESSAGE_ERROR_EXITO);
            
            return response;
            
        }catch(Exception ex){
            try {
                System.out.println("Exception: " + ex.getClass().getSimpleName() +" capturada");
                ex.printStackTrace();
                response.set(39, CODE_ERROR_EXCEPTION);
                response.set(63, MESSAGE_ERROR_EXCEPTION);
                return response;
            } catch (Exception ex1) {
                System.out.println("Error al empaquetar ISO");
                return response;
            }
        }
    }
    
    public ISOMsg buySoat(EmisionRequestDTO data, ISOMsg responseTemplate){
        ISOMsg response = responseTemplate;
        try{
            //Obtener token
            System.out.println("Obteniendo Token.... ");
            String token = client.getToken();

            //validando token
            if (token == null) {
                System.out.println("Error obteniendo Token =( .... ");
                response.set(39, CODE_ERROR_LOGIN);
                response.set(63, MESSAGE_ERROR_LOGIN);
                return response;
            }
            System.out.println("Token exitoso");

            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println("Obteniendo emisión de SOAT......");
            String emisionResponse = client.getEmision(data, token);
            
            if (emisionResponse == null) {
                System.out.println("Error obteniendo emision SOAT");
                response.set(39, CODE_ERROR_EMISION);
                response.set(63, MESSAGE_ERROR_EMISION);
                return response;
            }
            
            response.set(124, emisionResponse.trim());
            response.set(39, CODE_ERROR_EXITO);
            response.set(63, MESSAGE_ERROR_EXITO);
            
            
            return response;
            
        
        }catch(Exception ex){
             try {
                System.out.println("Exception: " + ex.getClass().getSimpleName() +" capturada");
                ex.printStackTrace();
                response.set(39, CODE_ERROR_EXCEPTION);
                response.set(63, MESSAGE_ERROR_EXCEPTION);
                return response;
            } catch (Exception ex1) {
                System.out.println("Error al empaquetar ISO");
                return response;
            }
        }
    }
    
    public ISOMsg makeRevoke(RevokeRequestDTO data, ISOMsg responseTemplate){
        ISOMsg response = responseTemplate;
        try{
            //Obtener token
            System.out.println("Obteniendo Token.... ");
            String token = client.getToken();

            //validando token
            if (token == null) {
                System.out.println("Error obteniendo Token");
                response.set(39, CODE_ERROR_LOGIN);
                response.set(63, MESSAGE_ERROR_LOGIN);
                return response;
            }
            System.out.println("Token exitoso");

            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println("Ejecutando reverso de SOAT......");
            RevokeResponseDTO revokeResponse = client.makeRevoke(data, token);
            
            if (revokeResponse == null) {
                System.out.println("Error reversando SOAT");
                response.set(39, CODE_ERROR_REVOKE);
                response.set(63, MESSAGE_ERROR_REVOKE);
                return response;
            }
            
            response.set(39, CODE_ERROR_EXITO);
            response.set(63, MESSAGE_ERROR_EXITO);
            
            response.set(41, revokeResponse.getRegistration().trim());
            response.set(82, revokeResponse.getIdentification().toString().trim());
            
            return response;
            
        
        }catch(Exception ex){
             try {
                System.out.println("Exception: " + ex.getClass().getSimpleName() +" capturada");
                ex.printStackTrace();
                response.set(39, CODE_ERROR_EXCEPTION);
                response.set(63, MESSAGE_ERROR_EXCEPTION);
                return response;
            } catch (Exception ex1) {
                System.out.println("Error al empaquetar ISO");
                return response;
            }
        }
    }

}
