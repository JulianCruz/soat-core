/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author JCruz
 */
public class ReadProperties {

    private final String path;
    private final Properties propiedades = new Properties();

    public ReadProperties(String path) {
        this.path = path;
        loadFile();
    }
    
    public String getProperty(String prop){
        try{
            System.out.println("Accede a: "+path);                 
            System.out.println("Buscando a: "+prop);                 
            System.out.println("Retornando: "+propiedades.getProperty(prop));                 
            return propiedades.getProperty(prop);
        }catch (Exception ex){
            System.out.println("Excepcion: " + ex.getClass().getSimpleName());
            ex.printStackTrace();
            return null;
        }
    }
    
    private void loadFile() {
        try {
            propiedades.load(new FileInputStream(path));
            System.out.println("Load true");
        } catch (FileNotFoundException e) {
            System.out.println("Error, El archivo de propiedaes "+ path +" no exite");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error, No se puede leer el archivo "+ path);
            e.printStackTrace();
        }
    }
}


