/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.util;

import java.util.Collection;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author JCruz
 */
public class ValidarUtil {
    
    public static boolean validISOMessage(ISOMsg message, Collection<Integer> fields){
        for(Integer i : fields){
            if(!message.hasField(i)){
                System.out.println("Error al validar entrada, hace falta el campo "+i);
                return false;
            }
        }
        return true;
    }
}
