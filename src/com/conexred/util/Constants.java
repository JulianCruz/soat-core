/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.util;

/**
 *
 * @author JCruz
 */
public class Constants {
    
    
    
    public static final String CODE_ERROR_EXITO =  "00";
    public static final String MESSAGE_ERROR_EXITO =  "SOLICITUD TRAMITADA CON EXITO";
    
    public static final String CODE_ERROR_LOGIN =  "90";
    public static final String MESSAGE_ERROR_LOGIN =  "NO ES POSIBLE INGRESAR A SOAT, INTENTE NUEVAMENTE";
    
    public static final String CODE_ERROR_QUERY =  "91";
    public static final String MESSAGE_ERROR_QUERY =  "ERROR AL REALIZAR CONSULTA DE SOAT, INTENTE NUEVAMENTE";
    
    public static final String CODE_ERROR_CAMPO =  "92";
    public static final String MESSAGE_ERROR_CAMPO =  "ERROR AL REALIZAR TRANSACCION, VERIFIQUE LOS DATOS E INTENTE NUEVAMENTE";
    
    public static final String CODE_ERROR_PROCESO =  "93";
    public static final String MESSAGE_ERROR_PROCESO =  "ERROR, PROCESO INVALIDO";
    
    public static final String CODE_ERROR_EMISION =  "94";
    public static final String MESSAGE_ERROR_EMISION =  "ERROR AL CONSUMIR EMISION CON SOAT, VERIFIQUE LOS DATOS E INTENTE NUEVAMENTE";
    
    public static final String CODE_ERROR_REVOKE =  "95";
    public static final String MESSAGE_ERROR_REVOKE =  "ERROR AL CONSUMIR REVOKE DE SOAT, VERIFIQUE LOS DATOS E INTENTE NUEVAMENTE";
    
    public static final String CODE_ERROR_EXCEPTION =  "99";
    public static final String MESSAGE_ERROR_EXCEPTION =  "ERROR EN LA CONSULTA, VERIFIQUE LOS DATOS E INTENTE NUEVAMENTE";
    
}
