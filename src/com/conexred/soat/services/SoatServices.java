/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.soat.services;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;

/**
 *
 * @author JCruz
 */
public class SoatServices implements Daemon{

    private BWGServer server = null;
    
    @Override
    public void init(DaemonContext dc) throws DaemonInitException, Exception {
        System.out.println("starting soat core service");
    }

    @Override
    public void start() throws Exception {
        System.out.println("Starting soat service at port: " + System.getProperty("config.port"));
        server = new BWGServer(Integer.parseInt(System.getProperty("config.port")), new SoatListener());
        server.init();
    }

    @Override
    public void stop() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void destroy() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
