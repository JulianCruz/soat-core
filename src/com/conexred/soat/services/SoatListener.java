/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.soat.services;

import com.conexred.dto.DataConnectionDTO;
import com.conexred.dto.EmisionRequestDTO;
import com.conexred.dto.RevokeRequestDTO;
import com.conexred.dto.RunInformationRequestDTO;
import com.conexred.process.ProcessSoat;
import static com.conexred.util.Constants.*;
import com.conexred.util.ReadProperties;
import com.conexred.util.ValidarUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.iso.ISOUtil;
import org.jpos.util.ThreadPool;

/**
 *
 * @author JCruz
 */
public class SoatListener implements ISORequestListener {

    private int MIN_THREAD;
    private int MAX_THREAD;
    private int TIME_OUT;
    private ThreadPool poolMessage;
    private final String CONFIG_FILE = "/opt/soat-core/config.properties";
    private DataConnectionDTO connectData ;

    public SoatListener() {
        try {
            ReadProperties scan = new ReadProperties(CONFIG_FILE);
            MIN_THREAD = Integer.parseInt(scan.getProperty("MIN_THREAD"));
            MAX_THREAD = Integer.parseInt(scan.getProperty("MAX_THREAD"));
            this.poolMessage = new ThreadPool(MIN_THREAD, MAX_THREAD);
            connectData = new DataConnectionDTO(scan.getProperty("url_base"), scan.getProperty("email"), scan.getProperty("key"));
            scan = null;
        } catch (Exception ex) {
            System.out.println("Excepcion: " + ex.getClass().getSimpleName());
            ex.printStackTrace();
        }
    }

    @Override
    public boolean process(ISOSource isos, ISOMsg isomsg) {
        boolean status = false;
        System.out.println("Ingreso process");
        try {
            poolMessage.execute(new Process(isos, isomsg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    class Process implements Runnable {

        private ISOSource source = null;
        private ISOMsg msg = null;
      
        
        public Process(ISOSource source, ISOMsg msg) {
            this.source = source;
            this.msg = msg;
            System.out.println("Creating process on soat listener");
        }
        
        @Override
        public void run() {
            System.out.println(".");
            System.out.println(".");
            System.out.println(".");
            System.out.println("*****************************************Inicio Transaccion*****************************************");
            ISOMsg response = null;
            Collection<Integer> fields = new ArrayList<>();
            
            try{
                System.out.println("Validando ISO de entrada");
                
                if(!this.msg.hasField(3)){
                    response.set(39, CODE_ERROR_CAMPO);
                    response.set(63, MESSAGE_ERROR_CAMPO);
                    source.send(response);
                    return;
                }
                
                response = (ISOMsg) this.msg.clone();
                response.setResponseMTI();
                ProcessSoat process = new ProcessSoat(connectData);
                
//                RunInformationRequestDTO queryRequest = new RunInformationRequestDTO("YSG86D", 80794978, "mauricio.suarez@puntored.co", 3114546180L);
                System.out.println("Seleccionando proceso");
                switch(ISOUtil.zeroUnPad(this.msg.getString(3))){
                    case "1":
                        
                        System.out.println("++++ Ingresando a consulta ++++");
                        //Campos esperados para trabajar
                        fields.add(32);
                        fields.add(42);
                        fields.add(43);
                        fields.add(44);
                        fields.add(98);
                        
                        if(!ValidarUtil.validISOMessage(this.msg,fields)){
                            response.set(39, CODE_ERROR_CAMPO);
                            response.set(63, MESSAGE_ERROR_CAMPO);
                            source.send(response);
                            System.out.println("++++ Saliendo de consulta ++++");
                            return;
                        }
                        
                        RunInformationRequestDTO queryRequest = new RunInformationRequestDTO(this.msg.getString(42).trim(), Long.parseLong(this.msg.getString(44).trim()), this.msg.getString(43).trim(), Long.parseLong(this.msg.getString(98).trim()),ISOUtil.zeroUnPad(this.msg.getString(32).trim()));
                        response = process.getInfo(queryRequest, response);
                        source.send(response);
                        System.out.println("++++ Saliendo de consulta ++++");
                        System.out.println(".");
                        System.out.println(".");
                        System.out.println(".");
                        System.out.println("*****************************************Fin Transaccion*****************************************");
                        break;
                    case "2":
                        System.out.println("++++ Ingresando a emisión ++++");
                        //Campos esperados para trabajar
                        fields.add(32);
                        fields.add(42);
                        fields.add(44);
                        fields.add(82);
                        fields.add(101);
                        
                        
                        if(!ValidarUtil.validISOMessage(this.msg,fields)){
                            response.set(39, CODE_ERROR_CAMPO);
                            response.set(63, MESSAGE_ERROR_CAMPO);
                            source.send(response);
                            System.out.println("++++ Saliendo de emision ++++");
                            return;
                        }
                        System.out.println("Campo de la fecha: " + this.msg.getString(101).trim());
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
                        Date fecha_compra = parser.parse(this.msg.getString(101).trim());                        
                        EmisionRequestDTO emisionRequest = new EmisionRequestDTO( Long.parseLong(this.msg.getString(82)), this.msg.getString(42), Long.parseLong(this.msg.getString(44)), dateFormatter.format(fecha_compra), ISOUtil.zeroUnPad(this.msg.getString(32).trim()));
                        response = process.buySoat(emisionRequest, response);
                        source.send(response);
                        System.out.println("++++ Saliendo de emision ++++");
                        System.out.println(".");
                        System.out.println(".");
                        System.out.println(".");
                        System.out.println("*****************************************Fin Transaccion*****************************************");
                        break;
                    case "3":
                        System.out.println("++++ Ingresando a revocacion ++++");
                        //Campos esperados para trabajar 
                        fields.add(42); //Registration     
                        fields.add(82); //Transaccion num
                        fields.add(83); //Identification
                        fields.add(84); //Sale_point
                        
                         if(!ValidarUtil.validISOMessage(this.msg,fields)){
                            response.set(39, CODE_ERROR_CAMPO);
                            response.set(63, MESSAGE_ERROR_CAMPO);
                            source.send(response);
                            return;
                        }
                        
                        RevokeRequestDTO revokeRequest = new RevokeRequestDTO(Long.parseLong(this.msg.getString(82)), this.msg.getString(42), Long.parseLong(this.msg.getString(83)), this.msg.getString(84));
                        response = process.makeRevoke(revokeRequest, response);
                       
                    default:
                        response.set(39, CODE_ERROR_LOGIN);
                        response.set(63, MESSAGE_ERROR_LOGIN);
                        source.send(response);
                        System.out.println(this.msg.getString(3));
                        break;
                
                }
                
            }catch(Exception ex){
                try {
                    System.out.println("Exception: "+ ex.getClass().getSimpleName() +" Capturada");
                    response.set(39, CODE_ERROR_EXCEPTION);
                    response.set(63, MESSAGE_ERROR_EXCEPTION);
                    source.send(response);
                    ex.printStackTrace();
                } catch (Exception ex1) {
                    System.out.println("Exception: "+ ex.getClass().getSimpleName() +" Capturada");
                    ex.printStackTrace();
                }
            }

        }


}

}
    
    

