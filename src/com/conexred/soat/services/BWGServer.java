/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.soat.services;

import com.conexred.util.ReadProperties;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ServerChannel;
import org.jpos.iso.channel.ASCIIChannel;
import org.jpos.iso.packager.ISO87APackager;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;
import org.jpos.util.ThreadPool;

/**
 *
 * @author JCruz
 */
public class BWGServer {
    
    private static int MIN_THREAD;
    private static int MAX_THREAD;

    private static ISOServer server = null;
    private static Thread proceso = null;
    private static int puerto = 0;
    private static ISORequestListener listener = null;

    public BWGServer(int port, ISORequestListener listener) {
        ReadProperties reader = new ReadProperties("/opt/soat-core/config.properties");
        MIN_THREAD = Integer.parseInt(reader.getProperty("MIN_THREAD"));
        MAX_THREAD = Integer.parseInt(reader.getProperty("MAX_THREAD"));
        this.puerto = port;
        this.listener = listener;
        reader = null;
    }

    public static void init() {
        PrintStream out = null;
        SimpleDateFormat sdf = null;
        System.out.println("Ingreso init");
        try {
            Logger logger = new Logger();
            ISOChannel clientSideChannel = new ASCIIChannel(new ISO87APackager());            
            sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
            out = new PrintStream(new FileOutputStream("/var/log/" + System.getProperty("config.service") + "_" + sdf.format(new Date()) + ".log"));
            logger.addListener(new SimpleLogListener(out));
            ThreadPool pool = null;
            
            pool = new ThreadPool(MIN_THREAD, MAX_THREAD);
            pool.setLogger(logger, "iso-pool-soat");
            server = new ISOServer(puerto, (ServerChannel) clientSideChannel, pool);
            server.setLogger(logger, "iso-server-soat");
            server.addISORequestListener(listener);
            proceso = new Thread(server);
            System.out.println("start .......... ");
            proceso.start();
        } catch (Exception e) {  
            System.out.println(" server soat " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void stop() {
        if(server != null){
            server.shutdown();
        }
    }
}
