/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.dto;

/**
 *
 * @author JCruz
 */
public class RuntInformationResponseDTO {
    private Integer transaction_num;
    private String insurance_company;
    private String registration;
    private String identification;
    private String identification_type;
    private String name;
    private String address;
    private String city;
    private String vehicle_class;
    private String vehicle_service;
    private String vehicle_cc;
    private String vehicle_engine;
    private String vehicle_serie;
    private String vehicle_vin;
    private String vehicle_passenger;
    private String vehicle_capacity;
    private String vehicle_brand;
    private String vehicle_line;
    private String vehicle_model;
    private Integer rate;
    private Integer price;

    public Integer getTransaction_num() {
        return transaction_num;
    }

    public void setTransaction_num(Integer transaction_num) {
        this.transaction_num = transaction_num;
    }

    public String getInsurance_company() {
        return insurance_company;
    }

    public void setInsurance_company(String insurance_company) {
        this.insurance_company = insurance_company;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getIdentification_type() {
        return identification_type;
    }

    public void setIdentification_type(String identification_type) {
        this.identification_type = identification_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVehicle_class() {
        return vehicle_class;
    }

    public void setVehicle_class(String vehicle_class) {
        this.vehicle_class = vehicle_class;
    }

    public String getVehicle_service() {
        return vehicle_service;
    }

    public void setVehicle_service(String vehicle_service) {
        this.vehicle_service = vehicle_service;
    }

    public String getVehicle_cc() {
        return vehicle_cc;
    }

    public void setVehicle_cc(String vehicle_cc) {
        this.vehicle_cc = vehicle_cc;
    }

    public String getVehicle_engine() {
        return vehicle_engine;
    }

    public void setVehicle_engine(String vehicle_engine) {
        this.vehicle_engine = vehicle_engine;
    }

    public String getVehicle_serie() {
        return vehicle_serie;
    }

    public void setVehicle_serie(String vehicle_serie) {
        this.vehicle_serie = vehicle_serie;
    }

    public String getVehicle_vin() {
        return vehicle_vin;
    }

    public void setVehicle_vin(String vehicle_vin) {
        this.vehicle_vin = vehicle_vin;
    }

    public String getVehicle_passenger() {
        return vehicle_passenger;
    }

    public void setVehicle_passenger(String vehicle_passenger) {
        this.vehicle_passenger = vehicle_passenger;
    }

    public String getVehicle_capacity() {
        return vehicle_capacity;
    }

    public void setVehicle_capacity(String vehicle_capacity) {
        this.vehicle_capacity = vehicle_capacity;
    }

    public String getVehicle_brand() {
        return vehicle_brand;
    }

    public void setVehicle_brand(String vehicle_brand) {
        this.vehicle_brand = vehicle_brand;
    }

    public String getVehicle_line() {
        return vehicle_line;
    }

    public void setVehicle_line(String vehicle_line) {
        this.vehicle_line = vehicle_line;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "RuntInformationResponseDTO{" + "transaction_num=" + transaction_num + ", insurance_company=" + insurance_company + ", registration=" + registration + ", identification=" + identification + ", identification_type=" + identification_type + ", name=" + name + ", address=" + address + ", city=" + city + ", vehicle_class=" + vehicle_class + ", vehicle_service=" + vehicle_service + ", vehicle_cc=" + vehicle_cc + ", vehicle_engine=" + vehicle_engine + ", vehicle_serie=" + vehicle_serie + ", vehicle_vin=" + vehicle_vin + ", vehicle_passenger=" + vehicle_passenger + ", vehicle_capacity=" + vehicle_capacity + ", vehicle_brand=" + vehicle_brand + ", vehicle_line=" + vehicle_line + ", vehicle_model=" + vehicle_model + ", rate=" + rate + ", price=" + price + '}';
    }
    
    
    
    
}
