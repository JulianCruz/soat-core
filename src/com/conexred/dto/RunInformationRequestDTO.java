/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.dto;

/**
 *
 * @author JCruz
 */
public class RunInformationRequestDTO {
    
    private String sale_point;
    private String registration;
    private long identification;
    private String email;
    private long phone;

    public RunInformationRequestDTO(String registration, long identification, String email, long phone, String sale_point) {
        this.registration = registration;
        this.identification = identification;
        this.email = email;
        this.phone = phone;
        this.sale_point = sale_point;
    }

    
    
    public String getSale_point() {
        return sale_point;
    }

    public void setSale_point(String sale_point) {
        this.sale_point = sale_point;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public long getIdentification() {
        return identification;
    }

    public void setIdentification(long identification) {
        this.identification = identification;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    
    
}
