/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.dto;

/**
 *
 * @author JCruz
 */
public class EmisionResponseDTO {
    private String registration;
    private Long identification;
    private String form_num;
    private String promotional_txt;

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getForm_num() {
        return form_num;
    }

    public void setForm_num(String form_num) {
        this.form_num = form_num;
    }

    public String getPromotional_txt() {
        return promotional_txt;
    }

    public void setPromotional_txt(String promotional_txt) {
        this.promotional_txt = promotional_txt;
    }

    @Override
    public String toString() {
        return "ResponseEmisionReponseDTO{" + "registration=" + registration + ", identification=" + identification + ", form_num=" + form_num + ", promotional_txt=" + promotional_txt + '}';
    }

    
}
