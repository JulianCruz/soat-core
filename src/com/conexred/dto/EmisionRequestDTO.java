/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.dto;

/**
 *
 * @author JCruz
 */
public class EmisionRequestDTO {
    private Long transaction_num;
    private String registration;
    private Long identification;
    private String init_date;
    private String sale_point;

    public EmisionRequestDTO(Long transaction_num, String registration, Long identification, String init_date, String sale_point) {
        this.transaction_num = transaction_num;
        this.registration = registration;
        this.identification = identification;
        this.init_date = init_date;
        this.sale_point = sale_point;
    }

    public Long getTransaction_num() {
        return transaction_num;
    }

    public void setTransaction_num(Long transaction_num) {
        this.transaction_num = transaction_num;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getInit_date() {
        return init_date;
    }

    public void setInit_date(String init_date) {
        this.init_date = init_date;
    }

    public String getSale_point() {
        return sale_point;
    }

    public void setSale_point(String sale_point) {
        this.sale_point = sale_point;
    }

    @Override
    public String toString() {
        return "RequestEmisionDTO{" + "transaction_num=" + transaction_num + ", registration=" + registration + ", identification=" + identification + ", init_date=" + init_date + '}';
    }
    
}
