/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.dto;

/**
 *
 * @author JCruz
 */
public class RevokeRequestDTO {
    private Long transaction_num;
    private String registration;
    private Long identification;
    private String sale_point;

    public RevokeRequestDTO(Long transaction_num, String registration, Long identification, String sale_point) {
        this.transaction_num = transaction_num;
        this.registration = registration;
        this.identification = identification;
        this.sale_point = sale_point;
        
    }

    public Long getTransaction_num() {
        return transaction_num;
    }

    public void setTransaction_num(Long transaction_num) {
        this.transaction_num = transaction_num;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    public String getSale_point() {
        return sale_point;
    }

    public void setSale_point(String sale_point) {
        this.sale_point = sale_point;
    }

    @Override
    public String toString() {
        return "RevokeRequestDTO{" + "transaction_num=" + transaction_num + ", registration=" + registration + ", identification=" + identification + ", sale_point=" + sale_point + '}';
    }
    
}
