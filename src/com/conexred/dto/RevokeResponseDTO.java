/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexred.dto;

/**
 *
 * @author JCruz
 */
public class RevokeResponseDTO {
    
    private String registration;
    private Long identification;

    public RevokeResponseDTO(String registration, Long identification) {
        this.registration = registration;
        this.identification = identification;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public Long getIdentification() {
        return identification;
    }

    public void setIdentification(Long identification) {
        this.identification = identification;
    }

    @Override
    public String toString() {
        return "RevokeResponse{" + "registration=" + registration + ", identification=" + identification + '}';
    }
    
    
}
